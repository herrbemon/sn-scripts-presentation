-- import biblioteki JSON
json = require "lua_scripts/json"

-- globalna zmienna z danymi smogu
smogData = nil;

-- informacje o playerze, pobieramy miasto żeby wiedzieć skąd smog brać
player = nil;

-- jaka wartosc smogu aktywuje plansze
-- 0 - bardzo dobry (zawsze)
-- 1 - dobry
-- 2 - umiarkowany
-- 3 - dostateczny
-- 4 - zły
-- 5 - bardzo zły
smogIndexTrigger = 1;

-- norma PM25 w polsce 25 ug/m^3
pm25Norm = 25;

-- norma PM10 w polsce 50 ug/m^3
pm10Norm = 50;

function run(slide, currentTime)

	if player == nil then 
		return false;
	end;
		
	-- pobranie z bazy/serwera danych smogowych
	-- string.lower - zmienia ciąg tekstowy na małe litery np. Warszawa -> warszawa
	-- player.miasto - nazwa miasta w którym player się znajduje
	smogData = storage:fetch("smog." .. string.lower(player.miasto));

	
	-- jesli ich nie ma to nie wyswietlamy
	if smogData == nil then
		return false;
	end;

	local sValue = json.decode(smogData.Value);
	 
	-- jesli wartosc jakosci powietrza jest >= temu co ustalilismy to wyswietlamy plansze
	if sValue.value.aqIndex >= smogIndexTrigger then 
		return true;
	end;
	
	return false
end


function data(slide,currentTime)
	
	local sValue = json.decode(smogData.Value).value;

	-- obliczenie wartosci procentowych powyzej normy w przedziale [0,max]
	local pm25Percent = math.max(0,math.floor(((sValue.values["PM2,5"] / pm25Norm)) * 100) - 100);
	local pm10Percent = math.max(0,math.floor(((sValue.values["PM10"] / pm10Norm)) * 100) - 100);

	local toReturn = { pm10 = pm10Percent, pm25 = pm25Percent };
		
	-- zwracamy do planszy FLASH dane
	 return json.encode(toReturn);
end

function before(slide,currentTime)

	-- przed załadowaniem planszy pobranie informacji o playerze
	local pData = storage:fetch("player.info");
	if pData ~= nil then 
		player = json.decode(pData.Value);
	end;
end

function after(slide,currentTime)

end

function misc()
    api:get('stats/live')
    http:get("http://remotead.com?report")

    local inRadius = player.ux && player.uy < 5 km od centrum Warszawy

    local wOlsztynie = player.miasto == 'olsztyn';

    if player.miasto == 'olsztyn' then 
        return "slajd1.wmv";
    else if player.miasto == 'wrocław' then 
        return "slajd2.wmv";
    end;

    if statistics:lastDisplay(slide.id) > 3 godziny then
        return true;
    end;

end