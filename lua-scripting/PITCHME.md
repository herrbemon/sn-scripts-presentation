## Skryptowanie playlist 
Krótki opis możliwości

---

## Stary system - pluginy
* Jest trudny w tworzeniu - wymaga pisania oddzielnych bibliotek, znajomości systemu i pisania za pomocą C#
* Aktualizacja softu - każdy nowy lub poprawka wymaga fizycznej aktualizacji.


---

## Stary system - pluginy
* **Pluginy nie wiedziały na jakiej maszynie się znajdują**
* Archaiczny cache - system plików XML.
* Brak kontroli nad danymi i slajdami - nie można tworzyć warunków wyświetleń, zależności od danych, działa tylko z plikami SWF.
---

### Dlaczego skrypty

* Nie da się wyklikać wszystkiego co chcemy, ma wystarczyć na lata
* Łatwe w tworzeniu - prosty język skryptowy, łatwa składnia, ukryta złożoność wewnątrz playera, nie trzeba kompilować.
* Uniwersalność - jeden skrypt do wielu slajdów, obsługa każdego typu np. SWF, FILM, OBRAZ
* **Wywalenie pisania skryptów AS do flashy**

---

### Dlaczego skrypty

* **Łatwa aktualizacja - wysyłamy playlisty nie całe playery**
* Trudne kampanie - bez problemu tworzenie trudnych kampanii, złożonych warunków wyświetleń zależnych od zewnętrznych dostawców
* Cache przeniesiony do bazy SQLite - poprawne dane, łatwy dostęp, statystyki itp.

---

### Przykłady - SMOG

* Jeden skrypt - możemy wyświetlić SWF dynamicznego, film lub cokolwiek zależnego od wartości smogu
* Skrypt wie od razu dla jakiego miasta pobrać automatycznie SMOG
* Nie martwimy się o rozesłanie slajdów na konkretne maszyny

+++?code=lua-scripting/smog_example.lua&lang=lua
@[67-74](Przed załadowanie slajdu pobranie informacji o playerze na którym wyświetlane są dane)
@[70](Pobranie danych - automatyczny cache, aktualne dane itp. z automatu)
@[25](Sprawdzenie czy slajd ma być wyświetlony czy nie)
@[34](Pobranie danych o smogu, **wiemy dla jakiego miasta** automatycznie !)
@[38-40](Jeśli danych dla miasta nie ma w ogóle  to **nie wyświetlamy slajdu** !)
@[45-47](Jeśli smog jest mniejszy niż chcemy też nie wyświetlamy slajdu wcale !)
@[53](Przekazanie danych do planszy flash - opcjonalne)
@[58-59](Obliczenie wartości procentowych przekroczenia normy)

+++

> Wygląda skomplikowanie ale to tylko:

$$procent = \lceil \max_{0 \leq procent }((\frac{smog}{norma} \cdot 100) - 100) \rceil$$

+++?code=lua-scripting/smog_example.lua&lang=lua

@[61](Zwrócenie odpowiednich danych do flasha)

+++?code=lua-scripting/smog_example.as3&lang=actionscript

@[1-7](Flash tylko sprawdza czy coś otrzymał)
@[6](Parsuje JSON-a)
@[10](Wyświetla wartość)
+++
### Przykład - SMOG - Plansza flash
> Te kilka lini flasha doda każdy. **Nie trzeba programować flasha ! - ten kod się nie zmienia**

---?code=lua-scripting/smog_example.lua&lang=lua
### Flow skryptu

@[67](Funkcja wywoływana przed załadowaniem slajdu)
@[76](Funkcja wywoływana po załadowaniu slajdu)
@[25](Funkcja zwracająca czy wyświetlić slajd czy nie)
@[53](Funkcja zwracająca dane (opcjonalnie))
@[53](Każda funkcja dostaje informacje o slajdzie i aktualnym czasie)

---
### Informacje o playerze

* Z automatu dostajemy
  * Nazwe np. `SN100`
  * Lokalizacje np. `LOK0001`
  * Miasto, województwo
  * UX i UY !

---
### Informacje o slajdzie

* Z automatu dostajemy
  * ID slajdu np. `2145225`
  * Pełną scieżkę i nazwę pliku
  * Typ (SWF, MOVIE ...)

---?code=lua-scripting/smog_example.lua&lang=lua

### Co możemy w skrypcie
@[70](Pobrać dowolne dane z systemu SN, cachowac i nie martwić się o nic)
@[81](Wywołać dowolną funkcję z serwer SN np. zaraportować statystyki, pobrać film)
@[82](Wywołać dowolny adres zenwnętrzny np. zewnętrznej agencji reklamowej - staty, raporty, zewnętrzne dane bezpośrednio od dostawcy)
@[84](Inne niestandardowe rzeczy np. sprawić aby slajd był wyświetlany tylko w lokalizacjach oddalonych o 5 km od centrum warszawy)
@[86](Wyświetlać tylko w olsztynie)
@[88-92](Wyświetlać różne slajdy w zależności od miasta ! Nie martwić się playlistą i wysyłką odpowiedniej do konkretnych lokalizacji !)
@[94-96](Wyświetlać slajdy co 3 godziny (i inne warianty np. 5 razy na godzinę, tylko raz itp.))

---

### Co możemy w skrypcie

* Automatyzować pracę
* Obsłużyć każdą niestandardową kampanię w krótkim czasie
* Prawdziwy programmatic
* Za darmo dostajemy statystyki w czasie rzeczywistym (jeśli jest internet)
 * Alerty wyświetleń / braku ??? 

---

### Materiały

* [LUA](https://www.lua.org/docs.html)

> Wymagane tylko operatory logiczne, arytmetyczne, kilka podstawowych funkcji np. math.*, string.* i 
> ogólna znajomość składni. **Do nauki w kilka godzin**

---

### Rozwój - automatyzacja kampanii

* Slajd z informacją o kampanii
* Automatyzacja wyświetlania - slajdy tylko w konkretnych miastach i dacie - 80% kampanii
* Zmiana sposobu rozsyłania slajdów - z automatu na maszyny w momencie dodania do oferty z kampanii.
* Skrypt pobiera informacje o kampaniach na maszynie i automatycznie wyświetla slajdy lub nie.
* 99% roboty idzie z automatu, tylko nadzorować.

